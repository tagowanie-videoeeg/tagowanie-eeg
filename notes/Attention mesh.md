https://arxiv.org/pdf/2006.10962.pdf

# Opis: 
Podejście do konstrukcji macierzy punktów opisującej geometrie twarzy w 3D. To podejście koncentruje się na regionach kluczowych (w naszym przypadku oczy/źrenice), które potrzebują bardziej szczegółowego opisu. 
Proces oparty jest na modelu:

Detekcja twarzy -> sieć regresyjna do charakterystyki punktów szczególnych -> macierz geometrii twarzy z naniesionym regionami kluczowymi 

Problem: Jeżeli sieć jako input dostanie całą twarz to zmniejszymy jakość w regionach kluczowych 

Rozwiązanie: 
1. Podejście kaskadowe - z pomocą pierwszego face mesha wycinamy część zdjęcia i przekazujemy do wyspecjalizowanej dla danego regionu **oddzielnej** sieci. 
2. Attention mesh - **jeden model** podzielony na submodele. Jeden do obliczania face mesh, ale już z naniesionymi krawędziami regionów kluczowych i równolegle działające submodele specyficzne dla wyznaczonych regionów.

Zaletą Attention mesh jest szybkość osiągnięta poprzez redukcję synchronizacji CPU-GPU oraz większa wydajność modelu dla regionów kluczowych. 

![](https://i.imgur.com/lkGHvFu.png)

W pracy opisane są również rozwiązania, które nas nie dotyczą. Mają one polepszyć synchronizacje submodeli różnych regionów kluczowych (oczy, usta).

# Attention mechanism 
Podział ze względu na mechanizm:
1. [Soft](https://arxiv.org/pdf/1502.04623.pdf)
2. [Hard](Max Jaderberg, Karen Simonyan, Andrew Zisserman, et al. Spatial transformer networks. In Advances in neural information processing systems, pages 2017–2025, 2015.)

Zasada działania:
Na zdjęcie nanoszona jest **mapa ROI** (region zainteresowania) w wymiarze 64x64. Nie mylić jej z face meshem. Dzięki niej można wyodrębnić poszczególne ROIe
i przekazać je do submodeli. 

Metody wyodrębniania: 
- 2D Gaussian kernel
- [[Affine transformation]]

# Affine transformation 
W tej pracy: 
Z mapy ROI 64x64 otrzymują ROI 24x24 za pomocą [spatial transformer module](https://papers.nips.cc/paper/2015/file/33ceb07bf4eeb3da587e268d663aba1a-Paper.pdf) Operacja ta jest kontrolowana przez macierz:

![](https://i.imgur.com/1nheZvh.png)

Pozwala to na obrót, zoom, translacje, zakrzywianie ROIa. Macierz konstruujemy z pomocą [supervised prediction of matrix parameters](https://scholar.google.pl/scholar?q=supervised+prediction+of+matrix+parameters&hl=pl&as_sdt=0&as_vis=1&oi=scholart) lub może być wyliczona z face mesh. 

![](https://i.imgur.com/vuYaIij.png)







