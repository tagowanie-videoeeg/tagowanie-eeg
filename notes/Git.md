## Wprowadzenie do Git 
Oparte na [artykule](https://www.freecodecamp.org/news/git-internals-objects-branches-create-repo/) z freeCodeCamp

### Materiał, który zostanie omówiony:
- [[#Obiekty]] : **blobs, trees,commits**
- **[[#Branches]]**
- **[[#Working directory]]**
- **[[#Staging area]]** 
- **[[#Repository]]**
 
## Obiekty 
Git służy do kontrolowania systemu plików. Wygodnie jest myśleć o tym, że prezentuje nam ostatni **snapshot** naszego systemu. 

**Blobs** to binarne obiekty wykorzystywane do przechowywania zawartości plików naszego systemu. Różnica pomiędzy plikiem i blobem polega na tym, że ten drugi nie zawiera **metadanych** (np. czas powstania)

Bloby są rozróżniane przez SHA-1 -> ciąg 20 bitów reprezentowanych przez 40 liczb w zapisie heksydecymalnym 

**Tree** jest odpowienikiem folderu. Rozpoznawane również przez SHA-1. 
