# Opis 
Publikacja ma na celu sformułowanie wytycznych do projektowania algorytmów deep-learningowych estymujących kierunek wzroku. Przeprowadza przegląd publicznie dostępnych [[# Zbiory danych|zbiorów danych]] oraz [[#Algorytmy Gaze Estimation|algorytmów]]. Proponują również sposób porównywania wyników z różnych prac naukowych. Zaimplementowane metody oraz kod dostępne są [tutaj](http://phi-ai.buaa.edu.cn/Gazehub/index.html)
****

# Gaze estimation
Opis podzielony jest na sekcje:
1. [[#Gaze estimation backround]] - rozwój i kategoryzacja metod estymacji
2. [[#Deep learning based method]] - opis rozwiązań state-of-the-art
3. [[#Zbiory danych]]
4. [[#Pre/post-porcessing methods]] - metody obróbki danych 
5. [[#Benchmark]] - sposób porównywania wyników z różnych prac naukowych 
6. [[#Wnioski]] - opinia na temat rozwoju metod deep learningowych oraz potencjalne kierunki rozwoju dla przyszłych prac 
****

## Gaze estimation backround
![[Pasted image 20220327105916.png]]
![](https://i.imgur.com/h32RoAk.png)

Na obrazie przedstawiona jest historia rozwoju metod estymacji kierunku spojrzenia. Pierwsze rozwiązania wykorzystywały zmiany potencjałów, odczytywane ze skóry na około oka. Następnie wykorzystywano urządzenia śledzące oko. Sprzęt do eye-trackingu umieszcza się na głowie lub w pewnej odległości. Metody estymacji oparte rozpoznawaniu obrazów można podzielić na trzy kategorie: 
1. [[#3D eye model recovery-based method]]
2. [[#2D eye feature regression-based method]]
3. [[#Appearance-based method]] - to na tej metodzie skupia się artykuł i to z niej będziemy korzystać w naszym projekcie. Szczegółowy opis rozpoczyna się [[#Szczegółowy opis metody Appearance-based|#tutaj]]

#### 3D eye model recovery-based method
- konstruuje model 3D oka
- model jest zwykle spersonalizowany - czyli wymaga wstępnej kalibracji 
- wymagają dodatkowego sprzętu (np. kamera na podczerwień)

#### 2D eye feature regression-based method
- zwykle wymagają tego samego sprzętu co modele 3D
- wykorzystują cechy geometryczne oka (położenie źrenicy itp) do ustalenia punktu spojrzenia (PoG)
- nie wymaga kalibracji 

#### Appearance-based method
- nie wymaga dodatkowego sprzętu (wystarczy kamera komputera)
- estymują kierunek spojrzenia za pomocą wyglądu oka 
- wymagają efektywnej ekstrakcji cech z obrazu 
- funkcji regresyjnej uczącej się przeprowadzania od cech ze zdjęcia do kierunku spojrzenia 
- dużego zbioru danych do trenowania funkcji 
****

#### Szczegółowy opis metody Appearance-based
Metody Appearance-based  bezpośrednio uczą [funkcję mapującą]() przeprowadzania od obrazu do kierunku spojrzenia. Korzystają z cech takich jak [piksele] lub [deep features]
Modele regresyjne: 
- [Sieci neuronowe]
- [Gaussian process regression model]
- [Adaptive linear regression model]
- [Konwolucyjne sieci neuronowe]

![](https://i.imgur.com/Dyle59l.png)

Głównymi przeszkodami napotykanymi przez modele są **ruchy głowy**, **różnice wewnątrzpopulacyjne**. Przeprowadzanie badań w różnych miejscach zwiększa poziom komplikacji. 

Najlepsze wyniki osiągane są przez Konwolucyjne sieci neuronowe. 

## Deep learning based method
Opis prowadzony jest z czterech perspektyw: 
- [[Ekstrakcja cech z inputu]]
- [[Architektura sieci neuronowych]]
- [[Kalibracja]]
- [[Urządzenia i platformy]]




# Zbiory danych 

# Algorytmy Gaze Estimation 
[]