Praca naukowa:
https://sci-hub.se/https://doi.org/10.1007/s11042-012-1220-z
DOI 10.1007/s11042-012-1220-z

Wstęp
Eye gazing estimation jako narzędzie do komunikacji pomiędzy człowiekiem i komputerem (HCI). 

Techniki pomiaru mogą mieć charakter inwazyjny / nieinwazyjny. Pierwsze wymagają, często drogiego sprzętu, który zmniejsza komfort użytkownika, ale są bardziej precyzyjne. 
	
Aktualne wysiłki skoncentrowane są wokół zwiększenia efektywności metod nieinwazyjnych. W tej pracy tworzony jest system do komunikacj z osobami sparaliżowanymi z wykorzystaniem podstawowej kamerki komputera. 

Kolejność: 
1) Skan całej twarzu użytkownika 
3) AAM - przypisywanie modelu kształtu do nowego obrazu. Wyodrębnienie rejonu oczu.
4) Klasyfikacja cech za pomocą SVM 
5) Ocena kierunku wzroku 


