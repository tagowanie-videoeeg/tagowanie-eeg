Definicja:
**Zbiór zasad i wartości, który ułatwia zarządzanie i tworzenie  rozbudowanego produktu. Zapewnia ujednolicenie metodyki pracy oraz określa cele krótko i długoterminowe**

Celem jest wytwarzenie "wartości" dla klienta w sposób ciągły i systematyczny.


Fundamenty, na których opiera się wspomniany w Definicji zbiór: 
- Empiryzm -> wraz z rozwojem projektu wykorzystujemy nabytą wiedzę do usprawnienia procesu. *Podejście adaptacyjne*
- Koncepcja lean -> precyzyjne określenie wartości oczekiwanej przez klienta i realizowanie jej w sposób płynny i oszczędny: "... a way to do more and more with less and less"


Filary empiryzmu: 
- Przejrzystość -> Zespół oraz klient mają wygodny dostęp do aktualnych informacji odnośnie stanu projektu i wykonywanych zadań
- Inspekcja -> **Regularna** weryfikacja postępów. Poszukiwanie optymalnych rozwiązań 
- Adaptacja -> Jak najszybsze wprowadzanie uzgodnionych rozwiązań
Przejrzystość jest konieczna do przeprowadzenia trafnej inspekcji i później adaptacji. Inspekcja i adaptacja odbywają się cyklicznie.


Scrum Team:
- Product Owner - odpowiedzialny za rozwój, kolejność wykonywanych zadań. Podejmuje decyzje odnośnie produktu. Maksymalizuje wartość produktu.
- Scrum Master - odpowiada za to żeby workflow był zgodny z założeniami Scram. Zarządza procesem powstawania produktu.
- Deweloperzy - zespół posiadający wszystkie umiejętności potrzebne do budowy produktu. Plan i wykonanie projektu.


Product Backlog:
Lista spełniająca warunki:
- Jest jedna
- Łatwo dostępna
- Uporządkowana 
- Zawiera elementy, które są dobrze opisane


Element listy zawiera: porządek, rozmiar, wartość 
Na górze listy znajdują się elementy, których opis jest dobrze opracowany. Są one gotowe do realizacji. Niżej umieszczane są mniej szczegółowo opisane zadania. 


Workflow: 
Scrum wprowadza iteracyjne podejście do organizacji pracy. Pojedyńczym cyklem jest **Sprint**

Sprint: (1 tydzień - 1 miesiąc)
1) Pobieramy zadania z Product Backlog
2) Tworzymy przyrost produktu (jakiś postęp) inaczej Increment
3) Pokazujemy przyrost klientom 
4) Uzyskujemy feedback
5) Aktualizujemy Backlog
6) Nowy sprint 

Increment:
- Zgodny z **definicją ukończenia**
- Zintegrowany z produktem
- Dobrze działający 
- Gotowy do prezentacji klientowi

Definicja ukończenia -> wspólnie ustalony standard. Determinuje czy zadanie trafia do Increment czy z powrotem na Backlog. 

Wydarzenia związane ze sprintem: 
1) Planowanie sprintu (max 8h):
- Ustalenie definicji ukończenia, tempa pracy, ilości zadań
- Sformułowanie celu sprintu
- Umieszczenie zadań w Product Backlog
2) Codzienny scrum (max 20 min)
- Stałe miejsce i czas 
- Co zrobiłem?
- Co zrobię? 
- Jakie widzę przeszkody? 
3) Przegląd sprintu (max 4h):
-  z udziałem klienta
-  prezentujemy Increment (demo produktu)
-  zbieramy feedback 
4) Retrospekcja sprintu (max 3h):
- Jak na podstawie ukończonego sprintu poprawić nadchodzący? 



