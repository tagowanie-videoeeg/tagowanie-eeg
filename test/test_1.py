'''pierwszy plik na testy'''
import eeg_tagging
from obci_readmanager.signal_processing.tags.read_tags_source import FileTagsSource


# na razie sprawdza tylko, czy jest w stanie pobrać nasz moduł
def test_import():
    import eeg_tagging
def test_export():
    list = [{'path':'1.jpg', 'x_tag' : 0, 'y_tag' : 0},
            {'path':'2.jpg', 'x_tag' : -1, 'y_tag' : 0},
            {'path':'3.jpg', 'x_tag' : 1, 'y_tag' : 1},
            {'path':'4.jpg', 'x_tag' : 1, 'y_tag' : 0},
            {'path':'5.jpg', 'x_tag' : 0, 'y_tag' : -1}]

    file = eeg_tagging.export.export(list)
    iterator = 0
    source = FileTagsSource(file)
    tags = source.get_tags()
    iterator = 0
    for tag in tags:
        if tag['name'] == 'looking':
            iterator +=1
    assert iterator == 1
    
def test_export_video():
    list = [{'timestamp':0.15,'x_tag' : 0, 'y_tag' : 0},
            {'timestamp':1.20, 'x_tag' : -1, 'y_tag' : 0},
            {'timestamp':1.25, 'x_tag' : 0, 'y_tag' : 0},
            {'timestamp':3.15,  'x_tag' : 0, 'y_tag' : 1},
            {'timestamp':3.20,  'x_tag' : -1, 'y_tag' : 0},
            {'timestamp':3.25,  'x_tag' : 0, 'y_tag' : 0},
            {'timestamp':3.26,  'x_tag' : 0, 'y_tag' : 0},
            {'timestamp':4.20,  'x_tag' : 0, 'y_tag' : 0}]

    file = eeg_tagging.export.export_video(list, 'video.mkv')
    source = FileTagsSource(file)
    tags = source.get_tags()
    iterator = 0
    for tag in tags:
        if tag['name'] == 'looking':
            iterator += 1
    assert iterator == 2
#def test_entrypoint():
    #os.system('tagger_video_eeg media/pictures...')


