import eeg_tagging as et
import os
from mediapipe import solutions
def main():
    path = et.argv[1]
    #inicjalizacja podstawowych narzędzi do oznaczania i rysowania facemeshy
    mp_drawing = solutions.drawing_utils
    mp_drawing_styles = solutions.drawing_styles
    drawing_spec = mp_drawing.DrawingSpec(thickness=1, circle_radius=1)
    mp_face_mesh = solutions.face_mesh

    #tworzy obiekt który będzie przeprowadzał facemeshowanie
    fm = mp_face_mesh.FaceMesh(
        static_image_mode=True, #czy statyczny
        max_num_faces=1,
        refine_landmarks=True,
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5)
    unlabeled_dir = os.path.abspath(os.path.join(os.path.dirname(path), os.path.join("unlabeled"))) #ścieżka do folderu ze zdjęciami bezlandmarków
    labeled_dir = os.path.abspath(os.path.join(os.path.dirname(path), os.path.join("labeled"))) #ścieżka do folderu ze zdjęciami zlandmarkami

    # tworzy liste ze ścieżkami do nieotagowanych zdjęć (dodatkowo + .gitignore folderu)
    # os.path.isfile() => dodatkowo się upewnia czy ścieżka z takim plikiem istnieje (zwraca True)
    IMAGE_FILES = [os.path.join(unlabeled_dir, f) for f in os.listdir(unlabeled_dir) if
                   os.path.isfile(os.path.join(unlabeled_dir, f))]
    IMAGE_FILES.pop(0) #usuwam .git ignore (alfanumerycznie dla .listdir zawsze będzie pierwszy)

    drawing_spec = mp_drawing.DrawingSpec()
    # et.ex.export(et.fpp.head_pos_pic(IMAGE_FILES, labeled_dir, show_pic=False))
    et.ex.export_video(et.fpp.head_pos_video(IMAGE_FILES, labeled_dir, show_film=False), labeled_dir)
if __name__ == "__main__":
    main()
