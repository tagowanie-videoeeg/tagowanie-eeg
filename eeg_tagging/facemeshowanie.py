import cv2
import mediapipe as mp
import os
import matplotlib.pyplot as plt

# #inicjalizacja podstawowych narzędzi do oznaczania i rysowania facemeshy
# mp_drawing = mp.solutions.drawing_utils
# mp_drawing_styles = mp.solutions.drawing_styles
# drawing_spec = mp_drawing.DrawingSpec(thickness=1, circle_radius=1)
# mp_face_mesh = mp.solutions.face_mesh
#
# #tworzy obiekt który będzie przeprowadzał facemeshowanie
# fm = mp_face_mesh.FaceMesh(
#     static_image_mode=True, #czy statyczny
#     max_num_faces=1,
#     refine_landmarks=True,
#     min_detection_confidence=0.5,
#     min_tracking_confidence=0.5)
#
# # __file__ => atrybut pokazujący ścieżke do pliku (.py) wywołującego
# # os.path.dirname(__file__) => ścieżka do folderu zawierającego
# # os.path.join(os.path.dirname(__file__), "..", r"media\pictures\unlabeled") => łączy wskazane w jedną ścieżke
# # ".." to cofnięcie do parent folder
# # os.path.abspath(path) => zwraca ścieżkę absolutną (normalizuje wskazaną ścieżke)
# unlabeled_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", os.path.join("media","pictures","unlabeled"))) #ścieżka do folderu ze zdjęciami bezlandmarków
# labeled_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", os.path.join("media","pictures","labeled"))) #ścieżka do folderu ze zdjęciami zlandmarkami
#
# # tworzy liste ze ścieżkami do nieotagowanych zdjęć (dodatkowo + .gitignore folderu)
# # os.path.isfile() => dodatkowo się upewnia czy ścieżka z takim plikiem istnieje (zwraca True)
# IMAGE_FILES = [os.path.join(unlabeled_dir, f) for f in os.listdir(unlabeled_dir) if
#                os.path.isfile(os.path.join(unlabeled_dir, f))]
# IMAGE_FILES.pop(0) #usuwam .git ignore (alfanumerycznie dla .listdir zawsze będzie pierwszy)
#
# drawing_spec = mp_drawing.DrawingSpec()

def get_face_landmarks(files):
    """
    Znajduje landmarki na wskazanej twarzy;
    files -> lista ze ścieżkami do zdjęć
    zwraca: listę z tuples (ścieżka do zdjęcia, obiektami z landmarkami) dla udanej analizy zdjęcia; liste z nazwami plików nieudanej analizy
    """
    no_face_detected = [] #lista z nazwami plików dla których nie rozpoznano twarzy
    results = [] #lista obiektami zawierającymi landmarki dla każdego zdjęcia
    with mp.solutions.face_mesh.FaceMesh(static_image_mode=True, max_num_faces=1, refine_landmarks=True, min_detection_confidence=0.5, min_tracking_confidence=0.5) as face_mesh:
        for image_path in files:
            #UWAGA u mnie cv2.imread() nie jest w stanie odczytać pliku ze ścieżki, wczytuje jedynie pliki znajdujące się w tym samym folderze, przechodzę na matplotliba
            # image = cv2.imread(image_path) #opencv wczytuje zdjęcię, UWAGA format BGR
            # rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) #mediapipe wymaga RGB => zamiana formatu
            rgb_image = plt.imread(image_path)
            result = face_mesh.process(rgb_image) #oznacza facial landmarki, zwraca OBIEKTY nie czyste współrzędne
            if result.multi_face_landmarks:
                results.append((image_path, result)) #jeśli udało to dodaje tuple (ścieżka do zdjęcia, obiekt z landmarkami) do listy wyników
            else:
                no_face_detected.append(os.path.basename(image_path)) #jeśli się nie udało (nie rozpoznał twarzy) dodaje nazwe pliku do listy nieudanych
    return results, no_face_detected

def draw_face_landmarks(files):
    for landmark_tuple in get_face_landmarks(files)[0]:
        file, landmarks = landmark_tuple
        # image = cv2.imread(file)
        image = plt.imread(file)
        annotated_image = image.copy()
        for face_landmarks in landmarks.multi_face_landmarks:
            mp_drawing.draw_landmarks(
                image=annotated_image,
                landmark_list=face_landmarks,
                connections=mp_face_mesh.FACEMESH_TESSELATION,
                landmark_drawing_spec=None,
                connection_drawing_spec=mp_drawing_styles
                    .get_default_face_mesh_tesselation_style())
            mp_drawing.draw_landmarks(
                image=annotated_image,
                landmark_list=face_landmarks,
                connections=mp_face_mesh.FACEMESH_CONTOURS,
                landmark_drawing_spec=None,
                connection_drawing_spec=mp_drawing_styles
                    .get_default_face_mesh_contours_style())
            mp_drawing.draw_landmarks(
                image=annotated_image,
                landmark_list=face_landmarks,
                connections=mp_face_mesh.FACEMESH_IRISES,
                landmark_drawing_spec=None,
                connection_drawing_spec=mp_drawing_styles
                    .get_default_face_mesh_iris_connections_style())

            path = os.path.abspath(os.path.join(labeled_dir, ('annotated_' + os.path.basename(file))))
            plt.imsave(path, annotated_image)

            #UWAGA mi cv2.imwrite ze ścieżką też nie działa, może zapisać ale tylko do folderu z którego jest skrypt
            #cv2.imwrite nawet jeśli ma błąd to nie wyrzuca errora tylko False stąd dla pewności
            # if not cv2.imwrite(path, annotated_image):
            #     raise Exception("Could not write image")

# if __name__ == "__main__":
#     draw_face_landmarks(IMAGE_FILES)