def is_looking(el):
    if el['x_tag'] == 0 and el['y_tag'] == 0:
        return 'looking'
    return 'not looking'

def export(list):

    from obci_readmanager.signal_processing.tags import tags_file_writer
    import os


    writer = tags_file_writer.TagsFileWriter(os.path.abspath(os.path.join(os.path.dirname(list[0]['path']),  ".." , os.path.join("export.tag"))), p_defs=None)

    for el in (list):
        tag = {'channelNumber': -1,
                       'start_timestamp': float(el['path'][-5]),
                       'end_timestamp': float(el['path'][-5]) + 1,
                       'name': is_looking(el),
                       'desc': {}
                       }
        writer.tag_received(tag)

    writer.finish_saving(0.0)
    return os.path.abspath(os.path.join(os.path.dirname(list[0]['path']),  ".." , os.path.join("export.tag")))

def export_video(list, path):

    from obci_readmanager.signal_processing.tags import tags_file_writer
    import os


    writer = tags_file_writer.TagsFileWriter(os.path.abspath(os.path.join(os.path.dirname(path),  ".." , os.path.join("export.tag"))), p_defs=None)
    start_timestamp = 0
    name = 'looking'
    for el in (list):
        end_timestamp = el['timestamp']
        if is_looking(el) is not name and float(end_timestamp) - float(start_timestamp) > 0.5:
            tag = {'channelNumber': -1,
                   'start_timestamp': start_timestamp,
                   'end_timestamp': end_timestamp,
                   'name': name,
                   'desc': {}
                   }
            start_timestamp = end_timestamp
            name = is_looking(el)
            writer.tag_received(tag)
    end_timestamp = list[-1]['timestamp']
    if float(end_timestamp) - float(start_timestamp) > 0.5:
        tag = {'channelNumber': -1,
               'start_timestamp': start_timestamp,
               'end_timestamp': end_timestamp,
               'name': is_looking(list[-1]),
               'desc': {}
               }

        writer.tag_received(tag)
    writer.finish_saving(0.0)
    return os.path.abspath(os.path.join(os.path.dirname(path),  ".." , os.path.join("export.tag")))








