from setuptools import setup, find_packages
import versioneer


INSTALL_REQUIREMENTS = {
    'setup': [
    ],
    'install': [
        'numpy==1.22.3',
        'mediapipe==0.8.9.1',
        'obci-readmanager == 1.1.3',
        'opencv-python == 4.5.4.60',
        'matplotlib == 3.5.2'
    ],
}

if __name__ == '__main__':
    setup(
        name='eeg_tagging',
        version=versioneer.get_version(),
        cmdclass=versioneer.get_cmdclass(),
        description='EEG tagging',
        zip_safe=False,
        author='Magdalena Marzec, Jadwiga Zdziarska, Szymon Luczko, Grzegorz Olszak',
        author_email='m.marzec10@student.uw.edu.pl',
        license='Other/Proprietary License',
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Natural Language :: English',
            'Topic :: Scientific/Engineering',
            'Intended Audience :: Developers',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.10',
        ],
        keywords='gaze vector estimation eeg',
        packages=find_packages(include=['eeg_tagging']),
        include_package_data=True,
        setup_requires=INSTALL_REQUIREMENTS['setup'],
        install_requires=INSTALL_REQUIREMENTS['install'],
        extras_require=INSTALL_REQUIREMENTS,
        entry_points={
            'console_scripts': ['tagger_video_eeg=eeg_tagging.main:main'],
        },
        ext_modules=[],
    )

